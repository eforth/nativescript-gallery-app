The Gallery is a NativeScript-built Android app for adding pictures to a single gallery. It is used to demonstrate the file uploading feature of the **Backendless MBaaS** and is used within the Mobile Apps Development course at **Vector Technology Institute**.

##Development##

This app is built with the NativeScript CLI. Once you have the [CLI installed](https://docs.nativescript.org/start/quick-setup), start by cloning the repo:

```
$ git clone https://eforth@bitbucket.org/eforth/nativescript-gallery-app.git
$ cd nativescript-gallery-app
```

Next, install the app's Android runtime, as well as the app's npm dependencies:

```
$ tns install
```

From there you can use the `run` command to run Gallery on Android:

```
$ tns run android --emulator
```

Finally, use the `livesync` command to push out changes to your app without having to go through the full build cycle:

```
$ tns livesync android --emulator --watch
```

##Screenshots##

![](screenshots/android-1.png)

