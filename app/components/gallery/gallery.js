var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var platformModule = require("platform");
var imagepickerModule = require("nativescript-imagepicker");
var permissionsModule = require( "nativescript-permissions" );
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModel = new Observable();

function onLoad(args) {
	var page = args.object;
	var items = new ObservableArray([]);

	items.push([
		{ url: "http://cdn.shopify.com/s/files/1/0169/5464/files/stranger-with-glasses_small.png?518" },
		{ url: "http://mirandapearsonpoetry.com/images/Books/amazon-logo-small.png" },
		{ url: "https://s3.amazonaws.com/go-public/image/go-logo-icon.small.png" },
		{ url: "https://s3.amazonaws.com/assets.mlh.io/events/logos/000/000/258/thumb/small-hacknotts-100x100.png?1442417699" },
		{ url: "http://screenshots.en.sftcdn.net/en/scrn/69712000/69712199/thumbnail_1444305019-100x100.png" },
		{ url: "https://media.licdn.com/mpr/mpr/shrinknp_100_100/AAEAAQAAAAAAAAJkAAAAJDQxMzE2NWI3LTQ0NGMtNDMwMC1hZGYwLTRmOGFjNjNhZjg2OQ.png" },
		{ url: "http://cdn.dexmedia.com/media/home-icon-2.png" },
		{ url: "http://www.eblida.org/Pictures/the-right-to-e-read/the-right-to-e-read-your-library_110x100.png" }
	]);

	viewModel.set("items", items);

    page.bindingContext = viewModel;
}

function gridViewItemTap(args) {
	console.log(JSON.stringify(items.getItem(args.index)));
}

function onAddItem() {

	var context = imagepickerModule.create({
        mode: "single"
    });
    
    if (platformModule.device.os === "Android" && platformModule.device.sdkVersion >= 23) {   
        permissionsModule.requestPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, "I need these permissions to read from storage")
        .then(function() {
            console.log("Permissions granted!");
            startSelection(context);
        })
        .catch(function() {
            console.log("Uh oh, no permissions - plan B time!");
        });
    } else {
        startSelection(context);
    }
}

function startSelection(context) {
    context
        .authorize().then(function() {
            return context.present();
        }).then(function(selection) {
            selection.forEach(function(selected) {
                console.log("uri: " + selected.uri);           
                console.log("fileUri: " + selected.fileUri);
            });
        }).catch(function (e) {
            console.log(e);
        });
}

exports.onLoad = onLoad;
exports.gridViewItemTap = gridViewItemTap;
exports.onAddItem = onAddItem;